package shape;

import java.util.Scanner;

public class Paint {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        printShapeChooseOptions();

        int shapeChoose;
        shapeChoose = scanNumberInRange(sc, 1, 3);

        Shape shape = getChosenShape(shapeChoose);
        printShapeColor(shape);
        int action = scanNumberInRange(sc, 1, 5);
        doAction(shape, action);
    }

    private static int scanNumberInRange(Scanner sc, int min, int max) {
        waitForNumberInput(sc);
        int number = sc.nextInt();
        while (number < min || number > max) {
            System.out.printf("Please enter number between %d and %d%n", min, max);
            waitForNumberInput(sc);
            number = sc.nextInt();
        }
        return number;
    }

    private static void waitForNumberInput(Scanner sc) {
        while (!sc.hasNextInt()) {
            sc.next();
            System.out.println("Please choose correct option");
        }
    }

    private static void doAction(Shape shape, int action) {
        if(action == 1){
            shape.paintColorShape("red");
        }else if(action == 2){
            shape.paintColorShape("green");
        }else if(action == 3){
            shape.paintColorShape("blue");
        }else if(action == 4){
            shape.paintColorShape("yellow");
        }else if(action == 5){
            shape.paintColorShape("black");
        }
    }

    private static Shape getChosenShape(int shapeChoose) {
        Shape shape = null;
        if (shapeChoose == 1) {
            shape = new Rectangle();
        } else if (shapeChoose == 2) {
            shape = new Circle();
        } else if (shapeChoose == 3) {
            shape = new Triangle();
        }
        return shape;
    }

    private static void printShapeChooseOptions() {
        System.out.println("Choose shape:");
        System.out.println("1 - Rectangle");
        System.out.println("2 - Circle");
        System.out.println("3 - Triangle");
    }

    private static void printShapeColor(Shape shape) {
        System.out.println("Choose color:");
        System.out.println("1 - red");
        System.out.println("2 - green");
        System.out.println("3 - blue");
        System.out.println("4 - yellow");
        System.out.println("5 - black");
    }

}
