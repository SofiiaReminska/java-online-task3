package shape;

import javax.swing.*;
import java.awt.*;

public abstract class Shape{
    public abstract void paintColorShape(String color);
}
