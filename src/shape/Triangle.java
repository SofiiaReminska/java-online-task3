package shape;

import java.awt.*;

public class Triangle extends Shape {
    private int sideLength = 0;

    public int getSideLength() {
        return sideLength;
    }

    public void setSideLength(int sideLength) {
        this.sideLength = sideLength;
    }

    public Triangle() {
        System.out.println("This is triangle!");
    }

    @Override
    public void paintColorShape(String color) {
        new Triangle();
        System.out.printf("It is %s :)%n", color);
    }
}
