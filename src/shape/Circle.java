package shape;

import javax.swing.*;
import java.awt.*;

public class Circle extends Shape {
    private int centerX = 0;
    private int centerY = 0;
    private int radius = 0;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }



    public Circle() {
        System.out.println("This is circle!");
    }

    @Override
    public void paintColorShape(String color) {
        new Circle();
        System.out.printf("It is %s :)%n", color);
    }
}
