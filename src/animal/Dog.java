package animal;

public class Dog extends Mammal {

    @Override
    public void walk() {
        System.out.println("Dog is walking !");
    }

    @Override
    public void eat() {
        System.out.println("Dod eats meat");
    }

    @Override
    public void sleep() {
        System.out.println("Dog sleep :)");
    }
}
