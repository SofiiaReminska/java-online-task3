package animal;

public class Carp extends Fish {

    @Override
    public void swim() {
        System.out.println("Carp swims ! ");
    }

    @Override
    public void sleep() {
        System.out.println("Carp sleep :)");
    }

    @Override
    public void eat() {
        System.out.println("Carp eats seaweed");
    }
}
