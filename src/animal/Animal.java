package animal;

public interface Animal {
    void eat();
    void sleep();
}
