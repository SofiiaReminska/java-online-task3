package animal;

import java.util.Scanner;

public class Zoo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        printAnimalCooseOptions();
        int animalChoose;
        animalChoose = scanNumberInRange(sc,1,4);
        Animal animal = getChosenAnimal(animalChoose);
        printAnimalActions(animal);
        int action = scanNumberInRange(sc,1,3);
        doAction(animal, action);
    }

    private static int scanNumberInRange(Scanner sc, int min, int max) {
        waitForNumberInput(sc);
        int number = sc.nextInt();
        while (number<min || number>max){
            System.out.printf("Please enter number between %d and %d%n", min, max);
            waitForNumberInput(sc);
            number = sc.nextInt();
        }
        return number;
    }

    private static void waitForNumberInput(Scanner sc) {
        while (!sc.hasNextInt()){
            sc.next();
            System.out.println("Please choose correct option");
        }
    }

    private static void doAction(Animal animal, int action) {
        if (action == 1) {
            animal.eat();
        } else if (action == 2) {
            animal.sleep();
        } else if (action == 3) {
            if (animal instanceof Mammal) {
                ((Mammal) animal).walk();
            } else if (animal instanceof Bird) {
                ((Bird) animal).fly();
            } else if (animal instanceof Fish) {
                ((Fish) animal).swim();
            }
        }
    }

    private static Animal getChosenAnimal(int animalChoose) {
        Animal animal = null;
        if (animalChoose == 1) {
            animal = new Carp();
        } else if (animalChoose == 2) {
            animal = new Parrot();
        } else if (animalChoose == 3) {
            animal = new Eagle();
        } else if (animalChoose == 4) {
            animal = new Dog();
        }
        return animal;
    }

    private static void printAnimalCooseOptions() {
        System.out.println("Choose animal:");
        System.out.println("1 - Carp:");
        System.out.println("2 - Parrot:");
        System.out.println("3 - Eagle:");
        System.out.println("4 - Dog:");
    }

    private static void printAnimalActions(Animal animal) {
        System.out.println("Choose action:");
        System.out.println("1 - eat");
        System.out.println("2 - sleep");
        if (animal instanceof Bird)
            System.out.println("3 - fly");
        else if (animal instanceof Mammal)
            System.out.println("3 - walk");
        else if (animal instanceof Fish)
            System.out.println("3 - swim");
    }

}
