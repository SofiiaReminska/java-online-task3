package animal;

public class Parrot extends Bird {
    private String meal = "seeds";

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    @Override
    public void eat() {
        System.out.printf("Parrot eats %s%n", getMeal());
    }

    @Override
    public void sleep() {
        System.out.println("Parrot sleep :)");
    }

    @Override
    public void fly() {
        System.out.println("Parrot can fly");
    }

}
