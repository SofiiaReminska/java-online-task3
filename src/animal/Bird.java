package animal;

public abstract class Bird implements Animal {
    @Override
    public void eat() {
        System.out.println("Bird eats seeds and insects");
    }

    @Override
    public void sleep() {
        System.out.println("Bird sleep :)");
    }

    public Bird() {
        System.out.println("This is bird!");
    }
    public abstract void fly();
}
