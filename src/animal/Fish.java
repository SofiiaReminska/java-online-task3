package animal;

public abstract class Fish implements Animal {
    @Override
    public void eat() {
        System.out.println("Fish eats algae and plankton");
    }

    @Override
    public void sleep() {
        System.out.println("Fish sleep :)");
    }

    public abstract void swim();

    public Fish() {
        System.out.println("This is fish!");
    }
}
