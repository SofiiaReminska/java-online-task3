package animal;

public abstract class Mammal implements Animal {
    private int paws = 4;

    public void setPaws(int paws) {
        this.paws = paws;
    }

    public int getPaws() {
        return paws;
    }

    public Mammal() {
        System.out.println("This is mammal!");
    }

    @Override
    public void eat() {
        System.out.println("Mammal eats meat and vegetables");
    }

    @Override
    public void sleep() {
        System.out.println("Mammal sleep :)");
    }

    public abstract void walk();
}
