package animal;

public class Eagle extends Bird {
    private String meal = "meat";

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    @Override
    public void sleep() {
        System.out.println("Eagle sleep :)");
    }

    @Override
    public void fly() {
        System.out.println("Eagle can fly");
    }

    @Override
    public void eat() {
        System.out.println("Eagle eats " + getMeal());
    }
}
